var eventList = [];

// DOM Ready
$(document).ready(function() {

	populateEventTable();

	//Event click
	$('#eventList table tbody').on('click', 'td a.detailview', showEventInfo);

});

function populateEventTable() {

	var tableContent = "";

	//jQuery AJAX call for JSON
	$.getJSON("/events/eventlist", function(data) {

		eventListData = data;

		// For each item in the JSON, add a table row and cell into the content string.
		$.each(data, function() {
			tableContent += "<tr>";
			tableContent += "<td> <a href='#' class='detailview' rel='" + this._id + "'>" + this.eventName + "</a></td>";
			tableContent += "<td>" + this.location + "</td>";
			tableContent += "<td>" + this.date + "</td>";
			tableContent += "<td>" + this.time + "</td>";
			tableContent += "</tr>";
		});

		// Inject the whole content string into the existing HTML table
		$("#eventList table tbody").html(tableContent);
	});
};

// Show User Info
function showEventInfo(event) {

	// Prevent link from firing
	event.preventDefault();

	// Retrieve username from link rel attribute
	var eventID = $(this).attr("rel");

	// Get the index of object based on id value
	var arrayPosition = eventListData.map(function(arrayItem) {
		return arrayItem._id;
	}).indexOf(eventID);

	// Get the event object
	var eventObject = eventListData[arrayPosition];

	//Populate info box
	$('#eventName').text(eventObject.eventName);
	$('#location').text(eventObject.location);
	$('#date').text(eventObject.date);
	$('#time').text(eventObject.time);
};